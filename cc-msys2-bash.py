#!/usr/bin/env python
# encoding: utf-8

from argparse import ArgumentParser

import itertools
import os
import subprocess


KEYS = ('HKEY_CLASSES_ROOT\Directory\shell',
        'HKEY_CLASSES_ROOT\Directory\Background\shell',
        'HKEY_CLASSES_ROOT\Drive\shell')

MENUS = ('MSYS2 Bash',
         'MSYS2 Bash (Administrator)')

ICON = 'msys2-bash.ico'


def install(args):
  if not 'Console_DIR' in os.environ:
    raise Exception("`%Console_DIR%` does not exist.")

  console_dir = '%Console_DIR%'

  if not 'Console_CONFIGURATION_DIR' in os.environ:
    raise Exception("`%Console_CONFIGURATION_DIR%` does not exist.")

  console_configuration_dir = '%Console_CONFIGURATION_DIR%'

  if not 'Hstart_BINARY_DIR' in os.environ:
    raise Exception("`%Hstart_BINARY_DIR%` does not exist.")

  hstart_binary_dir = '%Hstart_BINARY_DIR%'

  if not 'MSYS2_BINARY_DIR' in os.environ:
    raise Exception("`%MSYS2_BINARY_DIR%` does not exist.")

  msys2_binary_dir = '%MSYS2_BINARY_DIR%'

  for key, menu in itertools.product(KEYS, MENUS):
    key = key + '\\' + menu

    subprocess.call(('reg.exe', 'add',
                     key,
                     '/ve',
                     '/d', menu,
                     '/f'))

    subprocess.call(('reg.exe', 'add',
                     key,
                     '/v', 'Icon',
                     '/t', 'REG_EXPAND_SZ',
                     '/d', os.path.join(console_configuration_dir, ICON),
                     '/f'))

    command =  ('"' +
                os.path.join(console_dir, 'Console.exe') +
                '"' +
                ' ' +
                '-t' +
                ' ' +
                '"' +
                menu +
                '"' +
                ' -r "-l -i -c \\"cd \'%V\'; bash -i\\""')

    if menu == MENUS[1]:
      command = ('"' +
                 os.path.join(hstart_binary_dir, 'hstart.exe') +
                 '"' +
                 ' ' +
                 '/elevate' +
                 ' ' +
                 '"' +
                 command +
                 '"')

    subprocess.call(('reg.exe', 'add',
                     key + '\command',
                     '/ve',
                     '/t', 'REG_EXPAND_SZ',
                     '/d', command,
                     '/f'))


def uninstall(args):
  for key, menu in itertools.product(KEYS, MENUS):
    key = key + '\\' + menu

    subprocess.call(('reg.exe', 'delete',
                     key,
                     '/f'))


def main():
  parser     = ArgumentParser()
  subparsers = parser.add_subparsers()

  install_parser = subparsers.add_parser('install')
  install_parser.set_defaults(func = install)

  uninstall_parser = subparsers.add_parser('uninstall')
  uninstall_parser.set_defaults(func = uninstall)

  args = parser.parse_args()
  args.func(args)


if __name__ == '__main__':
  main()
