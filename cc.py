#!/usr/bin/env python
# encoding: utf-8

from argparse import ArgumentParser

import subprocess


def install(args):
  subprocess.call(('cc-command-prompt.py', 'install'), shell = True)
  subprocess.call(('cc-cygwin-bash.py',    'install'), shell = True)
  subprocess.call(('cc-git-bash.py',       'install'), shell = True)
  subprocess.call(('cc-msys-bash.py',      'install'), shell = True)
  subprocess.call(('cc-msys2-bash.py',     'install'), shell = True)


def uninstall(args):
  subprocess.call(('cc-command-prompt.py', 'uninstall'), shell = True)
  subprocess.call(('cc-cygwin-bash.py',    'uninstall'), shell = True)
  subprocess.call(('cc-git-bash.py',       'uninstall'), shell = True)
  subprocess.call(('cc-msys-bash.py',      'uninstall'), shell = True)
  subprocess.call(('cc-msys2-bash.py',     'uninstall'), shell = True)


def main():
  parser     = ArgumentParser()
  subparsers = parser.add_subparsers()

  install_parser = subparsers.add_parser('install')
  install_parser.set_defaults(func = install)

  uninstall_parser = subparsers.add_parser('uninstall')
  uninstall_parser.set_defaults(func = uninstall)

  args = parser.parse_args()
  args.func(args)


if __name__ == '__main__':
  main()
